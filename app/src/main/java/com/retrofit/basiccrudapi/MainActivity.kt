package com.retrofit.basiccrudapi

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isEmpty
import androidx.recyclerview.widget.LinearLayoutManager
import com.retrofit.basiccrudapi.model.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_data.*
import kotlinx.android.synthetic.main.add_data.view.*
import kotlinx.android.synthetic.main.edit_data.*
import kotlinx.android.synthetic.main.edit_data.view.*
import okhttp3.internal.http2.Http2Reader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create
import kotlin.coroutines.coroutineContext

val TAG = "MainActivity"

class MainActivity : AppCompatActivity(), DataAdapter.OnItemClickListener {

    private lateinit var adapter: DataAdapter
    val list = ArrayList<DataX>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = DataAdapter(this)
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(this)

        loadProgressBar()
        loadData()

        btn_fab.setOnClickListener{
            addData()
        }
    }

    private fun addData() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.add_data, null)
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
        val mAlertDialog = mBuilder.show()

        mDialogView.btn_add.setOnClickListener{

            val title = mDialogView.ad_et_title.text.toString()
            val body = mDialogView.ad_et_body.text.toString()
            val author = mDialogView.ad_et_author.text.toString()

            val service = RetrofitClient.retrofitInstance?.create(ApiRequests::class.java)
            val call = service?.createUser(title, body, author)

//            val newInfo = DataX(author, body, null , title)
//
//            adapter.add(newInfo)

            call?.enqueue(object : Callback<InsertItem> {
                override fun onResponse(call: Call<InsertItem>, response: Response<InsertItem>) {
                    if (title.isEmpty() || body.isEmpty() || author.isEmpty()) {
                        Toast.makeText(this@MainActivity, "Please fill all the fields", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this@MainActivity, "Successfully Saved", Toast.LENGTH_LONG).show()
                        val intent = intent
                        finish()
                        startActivity(intent)
                    }
                }

                override fun onFailure(call: Call<InsertItem>, t: Throwable) {
                    Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_SHORT).show()
                }

            })

        }
    }

    private fun loadData() {

        val service =RetrofitClient.retrofitInstance?.create(ApiRequests::class.java)
        val call = service?.getAll()

        call?.enqueue(object : Callback<newData> {
            override fun onResponse(call: Call<newData>, response: Response<newData>) {
                val body = response.body()
                if (body?.data != null) {
                    for ((index, model) in body.data.withIndex()) {
                        val newItem = DataX(
                                id = model.id,
                                title = model.title,
                                body = model.body,
                                author = model.author,
                        )
                        list.add(newItem)
                    }
                } else {
                    Toast.makeText(this@MainActivity, "No data found", Toast.LENGTH_SHORT).show()
                }
                adapter.addAll(list)
                btn_fab.visibility = View.VISIBLE
                progress_bar.visibility = View.GONE
            }

            override fun onFailure(call: Call<newData>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_SHORT).show()
            }

        })
    }

    fun loadProgressBar(){
        progress_bar.apply {
            progressMax = 100f
            setProgressWithAnimation(85f,1000)
            progressBarWidth = 5f
            backgroundProgressBarWidth = 2f
            progressBarColorStart = Color.GRAY
            progressBarColorEnd = Color.BLACK
        }
    }

    override fun onDeleteClick(position: Int, info: DataX) {

        message.text = info.toString()
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Are you sure?")
        builder.setMessage("Do you want to remove this?")
        builder.setPositiveButton("Yes") { dialog: DialogInterface, i: Int ->

            val itemId = info.id

            val service =RetrofitClient.retrofitInstance?.create(ApiRequests::class.java)
            val call = itemId?.let { service?.deleteByID(it, it) }

            call?.enqueue(object : Callback<deleteItem> {
                override fun onResponse(call: Call<deleteItem>, response: Response<deleteItem>) {
                    adapter.delete(position)
                    dialog.dismiss()
                    Toast.makeText(this@MainActivity, "Successfully deleted $itemId", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(call: Call<deleteItem>, t: Throwable) {
                    Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_SHORT).show()
                }
            })

        }
        builder.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int ->
            dialogInterface.dismiss()
        }
        builder.show()


    }

    override fun onEditClick(position: Int, info: DataX) {

        val itemId = info.id
        message.text = info.toString()

        val service = RetrofitClient.retrofitInstance?.create(ApiRequests::class.java)
        val call = itemId?.let { service?.getId(it) }

        call?.enqueue(object : Callback<UpdateItem> {
            override fun onResponse(call: Call<UpdateItem>, response: Response<UpdateItem>) {

                val body = response.body()
                //message.text = body.toString()

                val mDialogView = LayoutInflater.from(this@MainActivity).inflate(R.layout.edit_data, null)
                val mBuilder = AlertDialog.Builder(this@MainActivity)
                        .setView(mDialogView)
                val mAlertDialog = mBuilder.show()

                mDialogView.ed_et_title.text = Editable.Factory.getInstance().newEditable(body?.data?.title)
                mDialogView.ed_et_body.text = Editable.Factory.getInstance().newEditable(body?.data?.body)
                mDialogView.ed_et_author.text = Editable.Factory.getInstance().newEditable(body?.data?.author)

                mDialogView.btn_update.setOnClickListener {
                    mAlertDialog.dismiss()

                    var id = info.id
                    var title = mDialogView.ed_et_title.text.toString()
                    var body = mDialogView.ed_et_body.text.toString()
                    var author = mDialogView.ed_et_author.text.toString()

                    // recyclerview update
                    val updatedInfo = DataX(author, body, id, title)
                    adapter.edit(updatedInfo, position)

                    // query update
                    val service =RetrofitClient.retrofitInstance?.create(ApiRequests::class.java)
                    val call = service?.updateByID(id!!,id!!,title, author, body )

                    call?.enqueue(object : Callback<UpdateItem> {
                        override fun onResponse(call: Call<UpdateItem>, response: Response<UpdateItem>) {
                            Toast.makeText(this@MainActivity, "Book successfully Updated!", Toast.LENGTH_LONG).show()
                        }
                        override fun onFailure(call: Call<UpdateItem>, t: Throwable) {
                            Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_SHORT).show()
                        }
                    })
                }
            }

            override fun onFailure(call: Call<UpdateItem>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

}