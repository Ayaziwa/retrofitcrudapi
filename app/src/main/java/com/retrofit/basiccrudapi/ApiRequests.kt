package com.retrofit.basiccrudapi

import com.retrofit.basiccrudapi.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiRequests {

    //get all
    @GET("read.php")
    fun getAll(): Call<newData>

    // read by id
    @GET("read.php")
    fun getId(
            @Query("id")id: Int
    ): Call<UpdateItem>

    // insert
    @FormUrlEncoded
    @POST("insert.php")
    fun createUser(
            @Field("title") title: String,
            @Field("body") body: String,
            @Field("author") author: String
    ): Call<InsertItem>

    //update by id
    @FormUrlEncoded
    @POST("update.php/{id}")
    fun updateByID(
        @Path("id") id: Int,
        @Field("id") userId: Int,
        @Field("title") title: String,
        @Field("author") author: String,
        @Field("body") body: String
    ):Call<UpdateItem>

    // delete by id
    @FormUrlEncoded
    @POST("delete.php/{id}")
    fun deleteByID(
        @Path("id")id: Int,
        @Field("id") userId: Int
    ):Call<deleteItem>

    @DELETE("delete.php")
    fun deleteId(
            @Query("id") id: Int
    ): Call<deleteItem>


}