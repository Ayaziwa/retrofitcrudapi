package com.retrofit.basiccrudapi.model

data class deleteItem (val message: String,
                  val status: Boolean,
                  val data: Int)