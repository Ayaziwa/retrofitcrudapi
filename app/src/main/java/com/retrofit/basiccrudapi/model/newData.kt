package com.retrofit.basiccrudapi.model


import com.google.gson.annotations.SerializedName

data class newData(
    @SerializedName("data")
    val `data`: List<DataX>? = null,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)