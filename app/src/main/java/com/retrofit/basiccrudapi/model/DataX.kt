package com.retrofit.basiccrudapi.model


import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("author")
    val author: String,
    @SerializedName("body")
    val body: String,
    @SerializedName("id")
    val id: Int? ,
    @SerializedName("title")
    val title: String
)