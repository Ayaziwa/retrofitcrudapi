package com.retrofit.basiccrudapi.model

data class UpdateItem (
        val message: String,
        val status: Boolean,
        val data: DataX
)