package com.retrofit.basiccrudapi.model

import com.google.gson.annotations.SerializedName

data class InsertItem(
    val message: String,
    val status: Boolean
)