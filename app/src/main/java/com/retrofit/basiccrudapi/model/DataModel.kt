package com.retrofit.basiccrudapi.model

data class DataModel(
        val data: List<DataX>,
        val message: String,
        val status: Boolean
)