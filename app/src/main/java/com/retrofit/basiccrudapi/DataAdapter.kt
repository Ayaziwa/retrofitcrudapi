package com.retrofit.basiccrudapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.retrofit.basiccrudapi.model.DataX
import kotlinx.android.synthetic.main.custom_layout.view.*

class DataAdapter(private val listener:OnItemClickListener)
    : RecyclerView.Adapter<DataAdapter.ViewHolder>() {

    private var list: ArrayList<DataX> = arrayListOf()

    inner class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView),
        View.OnClickListener{

        var id: TextView = itemView.tv_id
        var title: TextView = itemView.tv_title
        var author: TextView = itemView.tv_author
        var body: TextView = itemView.tv_body

        val ivDelete: ImageView = itemView.iv_delete
        val ivEdit: ImageView = itemView.iv_edit
        override fun onClick(v: View?) {

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.custom_layout,
            parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]

        holder.id.text = data.id.toString()
        holder.title.text = data.title
        holder.author.text = data.author
        holder.body.text = data.body

        holder.ivEdit.setOnClickListener(){
            listener.onEditClick(position, data)
        }

        holder.ivDelete.setOnClickListener(){
            listener.onDeleteClick(position, data)
        }
    }

    override fun getItemCount(): Int = list.size

    fun addAll(listData: ArrayList<DataX>){
        list.addAll(listData)
        notifyDataSetChanged()
    }

    fun edit(info: DataX, position: Int){
        list[position] = info
        notifyDataSetChanged()
    }

    fun add(info: DataX){
        list.add(info)
        notifyDataSetChanged()
    }

    fun delete(position: Int){
        list.removeAt(position)
        notifyDataSetChanged()
    }

    fun clearData(){
        list.clear()
        notifyDataSetChanged()
    }

    interface OnItemClickListener{
        fun onDeleteClick(position: Int, info: DataX)
        fun onEditClick(position: Int, info: DataX)
    }

}